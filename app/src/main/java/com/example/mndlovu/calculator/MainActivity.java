package com.example.mndlovu.calculator;

import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private TextView displayCalculation;
    private TextView displayEquation;
    private String display = "";
    private String equationText = "";
    private Double answer = 0.0;
    private String[] equation = new String[3];
    private boolean solved = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayCalculation = findViewById(R.id.display_calculation);
        displayEquation = findViewById(R.id.display_equation);
    }

    public void pressedNumber(View view) {
        if (solved)
            displayEquation.setText("");
        Button button = (Button) view;
        String text = button.getText().toString();

        if (text.equals(".") && display.contains(".")) {
            displayCalculation.setText(display);
            return;
        }
        if (display.length() < 11) {
            display += text;
        }
        displayCalculation.setText(display);
        solved = false;
    }

    public void operator(View view) {
        if (display.equals(""))
            equation[0] = formatDouble(answer);
        else
            equation[0] = display;
        equationText = equation[0];
        display = "";
        solved = false;
        displayCalculation.setText(display);
        displayEquation.setText(equationText);
        Button b = (Button) view;
        String s = b.getText().toString();
        equation[1] = s;
        equationText += " " + s;
        displayEquation.setText(equationText);
    }

    private String formatDouble(Double d) {
        String str = d.toString();
        String[] splitNumber = str.replace(".", "e").split("e");
        if (splitNumber[1].equals("0")) {
            return splitNumber[0];
        }
        return str;
    }

    public void calculate(View view) {
        equation[2] = display;
        display = "";
        if (equation[0] == null || equation[2] == null)
            return;
        Double firstPart = Double.parseDouble(equation[0]);
        Double secondPart = Double.parseDouble(equation[2]);
        if (equation[1].contains("+"))
            answer = firstPart + secondPart;
        else if (equation[1].contains("-"))
            answer = firstPart - secondPart;
        else if (equation[1].contains("÷")) {
            answer = divide(firstPart, secondPart);
        } else if (equation[1].contains("x"))
            answer = firstPart * secondPart;
        if (secondPart != 0)
            displayEquation.setText(String.format("%s %s = ", equationText, equation[2]));
        displayCalculation.setText(formatDouble(answer));
        solved = true;
        reset();
    }

    private void reset() {
        for (int i = 0; i < 3; i++) {
            equation[i] = null;
        }
        display = "";
        equationText = "";
    }

    public void clearScreen(View view) {
        reset();
        answer = 0.0;
        solved = false;
        displayCalculation.setText(display);
        displayEquation.setText(equationText);
    }

    private Double divide(Double first, Double second) {
        if (second == 0) {
            displayEquation.setText("Can't divide by ");
            return 0.0;
        }
        return first / second;
    }

    public void deleteADigit(View view) {
        if (display.equals(""))
            return;
        display = display.substring(0, display.length() - 1);
        displayCalculation.setText(display);
    }

    public void absValue(View view) {
        if (solved) {
            display = formatDouble(answer * -1);
            displayCalculation.setText(display);
        } else if (!display.equals("")) {
            Double d = Double.parseDouble(display) * -1;
            display = formatDouble(d);
            displayCalculation.setText(display);
        }
    }
}
